
.PHONY: lalsuite clean boot configure compile metaio

# Use Clang compiler
# CC ?= clang
# CXX ?= clang++

_CFLAGS  = ${CFLAGS} -std=c17 -Wall -Wextra -Wno-error -Wno-implicit-function-declaration -I${PWD}/include
_LDFLAGS = -L${PWD}/lib ${LDFLAGS}

SOURCE_DIR  ?= "${PWD}"
INSTALL_DIR ?= "${PWD}"
USE_SWIG    ?= "ON"

# Install LALSuite in current directory
COPTS = ""
ifneq (${INSTALL_DIR},)
  PREFIX = "--prefix=${INSTALL_DIR}"
  COPTS = ${PREFIX}
endif

# Disable SWIG (USE_SWIG is OFF or empty)
ifeq (${USE_SWIG}, "OFF")
  COPTS += --disable-swig
endif

ifeq (${USE_SWIG},)
  COPTS += --disable-swig
endif

len := $(shell printf '%s' 'source `${PWD}/etc/lal-user-env.sh`' | wc -c)
spacer := $(shell printf "%$(len)s")

lalsuite: .lalsuite-build

	@cd vendor/lalsuite && $(MAKE) install
	@cp -Rf ${SOURCE_DIR}/cmake ${INSTALL_DIR}/share/

	@echo ""
	@echo " LALSuite library \033[0;32minstalled\033[0m into \033[0;32m${INSTALL_DIR}\033[0m"
	@echo "                 (headers located into \033[0;32m${INSTALL_DIR}/include\033[0m)"
	@echo ""
	@echo " \033[0;32mWhat's next ?\033[0m Define the following environment variable, if you want to use the library with cmake/autoconf"
	@echo "               (useful package provider available here: ./cmake)"
	@echo ""
	@echo "    \033[42m  ${spacer} \033[0m"
	@echo "    \033[42m  \`source ${INSTALL_DIR}/etc/lal-user-env.sh\` \033[0m"
	@echo "    \033[42m  ${spacer} \033[0m"
	@echo ""

.metaio-install: .metaio-build
	@cd vendor/metaio && $(MAKE) install
	@touch .metaio-install

examples: ./examples/main.cpp
	@cd examples && cmake .
	@cd examples && make

.lalsuite-boot: vendor/lalsuite/00boot
	@cd vendor/lalsuite && ./00boot
	@touch .lalsuite-boot

.lalsuite-configure: .metaio-install .lalsuite-boot
	@cd vendor/lalsuite && LDFLAGS="${_LDFLAGS}" CFLAGS="${_CFLAGS}" CC=${CC} CXX=${CXX} ./configure ${COPTS}
	@touch .lalsuite-configure 

.lalsuite-build: .lalsuite-configure
	@cd vendor/lalsuite && LDFLAGS="${_LDFLAGS}" CFLAGS="${_CFLAGS}" CC=${CC} CXX=${CXX} $(MAKE)

.metaio-boot: vendor/metaio/00boot
	@cd vendor/metaio && echo '\n' | ./00boot
	@touch .metaio-boot

.metaio-configure: .metaio-boot
	@cd vendor/metaio && ./configure ${PREFIX}
	@touch .metaio-configure 

.metaio-build: .metaio-configure
	@cd vendor/metaio && CXXFLAGS="${CXXFLAGS}" CFLAGS="${CFLAGS}" CC=${CC} CXX=${CXX} $(MAKE)
	@cd vendor/metaio && CXXFLAGS="${CXXFLAGS}" CFLAGS="${CFLAGS}" CC=${CC} CXX=${CXX} $(MAKE) install
	@touch .metaio-build


distclean: clean
clean:
	@$(RM) .lalsuite* .metaio*
	@make -sC vendor/lalsuite distclean || true
	@make -sC vendor/metaio distclean || true
