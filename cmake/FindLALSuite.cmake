# - Check for the presence of LALSuite
#
# The following variables are set when LALSuite is found:
#  LALSuite_FOUND       = Set to true, if all components of LALSuite
#                          have been found.
#  LALSuite_INCLUDE_DIRS   = Include path for the header files of LALSuite
#  LALSuite_LIBRARIES  = Link these to use LALSuite

## -----------------------------------------------------------------------------
## Check for the header files

find_path (LALSuite_INCLUDE_DIRS lal PATHS /usr/local/include /usr/include ${LAL_PREFIX}/include $ENV{LALSUITE}/include ${LALSUITE}/include $ENV{LALSuite}/include ${LALSuite}/include)

## -----------------------------------------------------------------------------
## Check for the library

find_library (LALSuite_LAL           NAMES lal           PATHS /usr/local/lib /usr/lib /lib $ENV{LAL_PREFIX}/lib $ENV{LALSUITE}/lib ${LALSUITE}/lib $ENV{LALSuite}/lib ${LALSuite}/lib)
find_library (LALSuite_LALINSPIRAL   NAMES lalinspiral   PATHS /usr/local/lib /usr/lib /lib $ENV{LAL_PREFIX}/lib $ENV{LALSUITE}/lib ${LALSUITE}/lib $ENV{LALSuite}/lib ${LALSuite}/lib)
find_library (LALSuite_LALMETAIO     NAMES lalmetaio     PATHS /usr/local/lib /usr/lib /lib $ENV{LAL_PREFIX}/lib $ENV{LALSUITE}/lib ${LALSUITE}/lib $ENV{LALSuite}/lib ${LALSuite}/lib)
find_library (LALSuite_LALBURST      NAMES lalburst      PATHS /usr/local/lib /usr/lib /lib $ENV{LAL_PREFIX}/lib $ENV{LALSUITE}/lib ${LALSUITE}/lib $ENV{LALSuite}/lib ${LALSuite}/lib)
#find_library (LALSuite_LALPULSAR     NAMES lalpulsar     PATHS /usr/local/lib /usr/lib /lib $ENV{LAL_PREFIX}/lib $ENV{LALSUITE}/lib ${LALSUITE}/lib $ENV{LALSuite}/lib ${LALSuite}/lib)
find_library (LALSuite_LALFRAME      NAMES lalframe      PATHS /usr/local/lib /usr/lib /lib $ENV{LAL_PREFIX}/lib $ENV{LALSUITE}/lib ${LALSUITE}/lib $ENV{LALSuite}/lib ${LALSuite}/lib)
find_library (LALSuite_LALSIMULATION NAMES lalsimulation PATHS /usr/local/lib /usr/lib /lib $ENV{LAL_PREFIX}/lib $ENV{LALSUITE}/lib ${LALSUITE}/lib $ENV{LALSuite}/lib ${LALSuite}/lib)
find_library (LALSuite_LALINFERENCE  NAMES lalinference  PATHS /usr/local/lib /usr/lib /lib $ENV{LAL_PREFIX}/lib $ENV{LALSUITE}/lib ${LALSUITE}/lib $ENV{LALSuite}/lib ${LALSuite}/lib)
find_library (LALSuite_LALSUPPORT    NAMES lalsupport    PATHS /usr/local/lib /usr/lib /lib $ENV{LAL_PREFIX}/lib $ENV{LALSUITE}/lib ${LALSUITE}/lib $ENV{LALSuite}/lib ${LALSuite}/lib)

if (NOT "${LALSuite_LAL}" STREQUAL "LALSuite_LAL-NOTFOUND")
set(LALSuite_LIBRARIES ${LALSuite_LIBRARIES} ${LALSuite_LAL})
endif()

if (NOT "${LALSuite_LALINSPIRAL}" STREQUAL "LALSuite_LALINSPIRAL-NOTFOUND")
set(LALSuite_LIBRARIES ${LALSuite_LIBRARIES} ${LALSuite_LALINSPIRAL})
endif()

if (NOT "${LALSuite_LALMETAIO}" STREQUAL "LALSuite_LALMETAIO-NOTFOUND")
set(LALSuite_LIBRARIES ${LALSuite_LIBRARIES} ${LALSuite_LALMETAIO})
endif()

if (NOT "${LALSuite_LALBURST}" STREQUAL "LALSuite_LALBURST-NOTFOUND")
set(LALSuite_LIBRARIES ${LALSuite_LIBRARIES} ${LALSuite_LALBURST})
endif()

#if (NOT "${LALSuite_LALPULSAR}" STREQUAL "LALSuite_LALPULSAR-NOTFOUND")
#set(LALSuite_LIBRARIES ${LALSuite_LIBRARIES} ${LALSuite_LALPULSAR})
#endif()

if (NOT "${LALSuite_LALFRAME}" STREQUAL "LALSuite_LALFRAME-NOTFOUND")
set(LALSuite_LIBRARIES ${LALSuite_LIBRARIES} ${LALSuite_LALFRAME})
endif()

if (NOT "${LALSuite_LALSIMULATION}" STREQUAL "LALSuite_LALSIMULATION-NOTFOUND")
set(LALSuite_LIBRARIES ${LALSuite_LIBRARIES} ${LALSuite_LALSIMULATION})
endif()

if (NOT "${LALSuite_LALINFERENCE}" STREQUAL "LALSuite_LALINFERENCE-NOTFOUND")
set(LALSuite_LIBRARIES ${LALSuite_LIBRARIES} ${LALSuite_LALINFERENCE})
endif()

if (NOT "${LALSuite_LALSUPPORT}" STREQUAL "LALSuite_LALSUPPORT-NOTFOUND")
set(LALSuite_LIBRARIES ${LALSuite_LIBRARIES} ${LALSuite_LALSUPPORT})
endif()

## -----------------------------------------------------------------------------
## Actions taken when all components have been found

if (LALSuite_INCLUDE_DIRS AND LALSuite_LIBRARIES)
  set (LALSuite_FOUND TRUE)
else (LALSuite_INCLUDE_DIRS AND LALSuite_LIBRARIES)
  if (NOT LALSuite_FIND_QUIETLY)
    if (NOT LALSuite_INCLUDE_DIRS)
      message (STATUS "Unable to find LALSuite header files!")
    endif (NOT LALSuite_INCLUDE_DIRS)
    if (NOT LALSuite_LIBRARIES)
      message (STATUS "Unable to find LALSuite library files!")
    endif (NOT LALSuite_LIBRARIES)
  endif (NOT LALSuite_FIND_QUIETLY)
endif (LALSuite_INCLUDE_DIRS AND LALSuite_LIBRARIES)

if (LALSuite_FOUND)

  if (NOT LALSuite_FIND_QUIETLY)
    message (STATUS "Found components for LALSuite")
    message (STATUS "LALSuite_INCLUDE_DIRS = ${LALSuite_INCLUDE_DIRS}")
    message (STATUS "LALSuite_LIBRARIES    = ${LALSuite_LIBRARIES}")
  endif (NOT LALSuite_FIND_QUIETLY)
else (LALSuite_FOUND)
  if (LALSuite_FIND_REQUIRED)
    message (FATAL_ERROR "Could not find LALSuite! (Maybe define LALSUITE environment variable to root directory")
  endif (LALSuite_FIND_REQUIRED)
endif (LALSuite_FOUND)

mark_as_advanced (
  LALSuite_FOUND
  LALSuite_LIBRARIES
  LALSuite_INCLUDE_DIRS
)
