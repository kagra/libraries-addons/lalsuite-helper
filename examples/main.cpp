#include <iostream>

#include <gsl/gsl_rng.h>
#include <lal/LALStdlib.h>
#include <lal/FrequencySeries.h>
#include <lal/TimeSeries.h>
#include <lal/Units.h>
#include <lal/LALSimNoise.h>
#include <lal/Date.h>

//
// Example taken from:
// https://lscsoft.docs.ligo.org/lalsuite/lalsimulation/group___l_a_l_sim_noise__c.html
//

int main(int argc, char** argv)
{
	std::cout << "Hello World !" << std::endl << std::endl;

	const int maxIter = 2;
	const double flow = 40.0; // 40 Hz low frequency cutoff
	const double duration = 1.0; // 1 second segments
	const double srate = 16.0; // sampling rate in Hertz
	size_t length = duration * srate; // segment length
	size_t stride = length / 2; // stride between segments
	LIGOTimeGPS epoch = { 0, 0 };
	REAL8FrequencySeries *psd;
	REAL8TimeSeries *seg;
	
	gsl_rng *rng;
	gsl_rng_env_setup();
	rng = gsl_rng_alloc(gsl_rng_default);
	
	seg = XLALCreateREAL8TimeSeries("strain", &epoch, 0.0, 1.0/srate, &lalStrainUnit, length);
	psd = XLALCreateREAL8FrequencySeries("psd", &epoch, 0.0, 1.0/duration, &lalSecondUnit, length/2 + 1);

	XLALSimNoisePSD(psd, flow, XLALSimNoisePSDiLIGOSRD);
	XLALSimNoise(seg, 0, psd, rng); // first time to initialize

	int i = 0;
	while(i++ < maxIter) {

		double t0 = XLALGPSGetREAL8(&seg->epoch);
		size_t j;
		for (j = 0; j < stride; ++j) // output first stride points
			printf("Data #%zu: %.9f\t%e\n", j, t0 + j*seg->deltaT, seg->data->data[j]);

		XLALSimNoise(seg, stride, psd, rng); // make more data
		std::cout << std::endl;
	}

	return 0;
}
