# LALSuite C++ Helper

## Overview

The LALSuite C++ Helper repository is designed to assist in the compilation of the LALSuite library for Unix and MacOS.
This repository simplifies the compilation process for LALSuite and provides helper scripts and configurations.

## Optional Packages
Optional packages include HDF5, SWIG, and CUDA.

## Installation

To install, first clone the repository:
```sh
# git clone https://git.ligo.org/kagra/libraries-addons/lalsuite-helper.git
# ```
Navigate to the project directory:
```sh
cd lalsuite-helper
```

Run the following commands to configure, compile, and install:
```sh
make configure
make
make install
```

## Usage
Instructions on how to use the helper scripts and configurations to compile LALSuite.

## Contributing
Contributions are welcome! Please fork the repository and create a pull request with your changes.

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Contact
For more information, please contact the repository maintainer.
